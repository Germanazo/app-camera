import {Component} from '@angular/core';
import {NavController, MenuController, Platform, Events} from 'ionic-angular';
import {GoogleMap, GoogleMapsEvent, GoogleMapsLatLng, GoogleMapsMarkerOptions} from 'ionic-native';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  protected map: GoogleMap;

  constructor(private navController: NavController,private platform: Platform, private events: Events) {
    this.platform.ready().then(() => this.onPlatformReady() );
  }

  private onPlatformReady(): void {
    this.map = new GoogleMap('map_canvas');

    GoogleMap.isAvailable().then(() => {

      // FIX MENU OPEN ERROR
      this.events.subscribe('menu:opened', () => this.map.setClickable(false));
      this.events.subscribe('menu:closed', () => this.map.setClickable(true));

      this.map.setMyLocationEnabled(true);
      this.map.setTrafficEnabled(true);

      this.map.setBackgroundColor('#000000');

      let myPosition = new GoogleMapsLatLng('38.9072', '-77.0369');
      this.map.setCenter(myPosition);

      this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((data: any) => {
        alert("Tapped map: " + JSON.stringify(data));
      });
    })
  }

  /*
  ionViewLoaded() {
    this.map = new GoogleMap('map_canvas');

    GoogleMap.isAvailable().then(() => {

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
        () => this.onMapReady(),
        () => alert("Error: onMapReady")
      );

      this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
        console.log("GoogleMap.onMapReady(): " + JSON.stringify(data));

        let myPosition = new GoogleMapsLatLng('38.9072', '-77.0369');
        console.log("My position is", myPosition);

        this.map.animateCamera({ target: myPosition, zoom: 10 });
      });
    });
  }*/

  private onMapReady(): void {

   console.log("MAP READY ?");
  }
}
