import {Component, ViewChild} from '@angular/core';
import {ionicBootstrap, Platform, MenuController, Nav, Events} from 'ionic-angular';
import {StatusBar} from 'ionic-native';

import {HomePage} from './pages/home/home';
import {HelloPage} from './pages/hello/hello';




@Component({
  templateUrl: 'build/app.html'
})
class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform, private menu: MenuController, private events: Events) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Map', component: HomePage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage },
      { title: 'Hello', component: HelloPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  menuClosed() {
    this.events.publish('menu:closed', '');
  }

  menuOpened() {
    this.events.publish('menu:opened', '');
  }
}

ionicBootstrap(MyApp);
